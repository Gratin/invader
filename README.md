##### Installing

Clone repo and cd into the directory. The run `sh install.sh`

---

### Usage

You can either add the site to the invader library file by using
`invader add` which will prompt you with both the site name and the site path. It will also create a `.invader` in the folder if it doesn't already exists.

Alternatively, you can simply create an `.invader` file in your site directory. However, you won't be able to use the site argument in the deploy function if it was not `added` via the `add` command.

`invader deploy` takes the sitename as the argument. You can deploy any sites from anywhere using this command, like so  
`invader add`  
`SomeSite`  
`/var/www/somesite`  
`invader deploy somesite`  

If you want to send a Slack message in one line, you can use
`invader deploy somesite yes "This is a custom Slack message"`
If the last argument is left empty, it will use the last commit message.
You can also invoke simply `invader` in the folder you wish to deploy.

---

### Configurations

There are three files added to you home directory.


By default, Invader ignoes all `.git` folder and uses the local `.gitignore` file to ignore files also.


* `.invaderlib` Contains the site library.


* `.invaderignore` Use like a .gitignore file, will be use an --exlude flag in the rsync call so all files within the `.invaderignore` will be ignored.


* `.invaderconfig` Thie file can contains default configuration, such as if all remote site are on a single server, you could set the REMOTE variable in this file.

---

### The .invader file and .invaderconfig

Inside the `.invader` file you should set values such as.


* `REMOTE` The target server address (IP or DNS).


* `SOURCE` The source directory to rsync from.


* `TARGET` The target directory to rsync to.


* `SSH_USER` The ssh user to use to rsync to the server.


* `BEFORE_LSCRIPTS` Local scripts to run prior to deploying. Delimiter is comma.


* `AFTER_LSCRIPTS` Local scripts after deploying. Delimiter is comma.


* `SLACK_ENDPOINT` The Slack channel webhook url.


The format is as folows `VARIABLE=VALUE`.

## Sample .invaderconfig
```
# Folder to upload
SOURCE="./"


#APPNAME used in the default Slack Message. If empty, defaults to folder name
APPNAME="My project"


# Directory to upload to on server
TARGET="/var/www/testing.sometest.com/httpdocs"


# SSH config name to rsync into server
SSHCONFIG="testconfig"


# Scripts to run before deploying
BEFORE_LSCRIPTS="vendor/bin/phpunit test,yarn build:production"


# Slack webhook to send message to
SLACK_ENDPOINT="https://hooks.slack.com/services/ChannelId/Etc."
```
