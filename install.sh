#!/bin/bash
echo "Installing Invader Deployer"
echo "Copying config files to home directory"

oldinstall="0"
if [ -f "$HOME/.invader" ]; then
	mv "$HOME/.invader" "$HOME/.invaderoldconfig"
	oldinstall="1"
fi

if [ ! -d "$HOME/.invader" ]; then
	mkdir $HOME/.invader
fi

if [[ "$oldinstall" == "1" ]]; then
	mv "$HOME/.invaderoldconfig" "$HOME/.invader/.invader"
fi

cp dist/.invaderconfig $HOME/.invader/.invaderconfig
cp dist/.invaderlib $HOME/.invader/.invaderlib
cp dist/.invaderignore $HOME/.invader/.invaderignore

echo "Installing the executable for user (Must be run as sudo)"
sudo cp src/invader /usr/local/bin/invader
sudo chmod u+x /usr/local/bin/invader

echo "Installation complete"
exit