#!/bin/bash
#DIR = folder form which the function is called
DIR="$(pwd)"
if [[ -z "$HOME" ]]; then
    HOME="~"
fi
CONFIGFILE="$HOME/.invader/.invaderconfig"
ENV=$1
CWD=$(basename "$DIR")
SENDTOSLACK=$3

if [[ -z "$ENV" ]]; then
    ENV="staging"
fi

if [[ -z "$CONFIGFILE" ]]; then
    echo "Veuillez creer un fichier de configuration (.invaderconfig)"
    exit
fi


#Load base config file into VAR=VALUE
source $CONFIGFILE

if [[ -z "$APPNAME" ]]; then
    APPNAME="$CWD"
fi

#Local config file
SYNCFILE="$DIR/.invader"
#NOT WORKING :( BUT KEEPING IT to REMEMBER MY FAILURES
PWDFILE="$DIR/.invaderpassword"
#List of file to ignore by default
BASEIGNOREFILE="$HOME/.invaderignore"
#List of file to ignore by default
LOCALIGNORE="$DIR/.invaderignore"
#Save sites library
LIBFILE=$HOME/.invader/.invaderlib
 

if [[ -z "$BASEIGNOREFILE" ]]; then
    touch "$BASEIGNOREFILE"
fi

if [[ -z "$LOCALIGNORE" ]]; then
    touch "$LOCALIGNORE"
fi

COMMAND=$1

if [ "$COMMAND" == "help" ]
then
    echo "Config path $CONFIGFILE"
    echo "Commands:"
    echo "   add        Interactive prompt to add a site"
    echo "   deploy     Takes and argument (site name) and ask you to valid the deployment"
    echo "   list       List all sites and path"
    exit
fi

# When we want to list our sites
# Todo Make prettier
if [ "$COMMAND" == "list" ]
then
    while IFS="=" read -ra line; do
        echo "${line[0]} :: ${line[1]}"
    done < $LIBFILE
    exit
fi

# When adding a site to our library
if [ "$COMMAND" == "add" ]
then
    echo "Enter site name"
    read sitename

    printf "Enter site path (empty for $DIR) \n"
    read -e sitepath

    if [ "$sitepath" == "" ]
    then
        sitepath=$DIR
    fi

    if [ ! -f "$sitepath/.invader" ]
    then
        echo "Creating invader file"
        touch $sitepath/.invader
    fi

    # Save into library file
    echo $sitename"="$sitepath >> $LIBFILE
    echo "Saved"
    exit
fi


sitefolder=""
if [ "$COMMAND" == "deploy" ]
then
    # Arg #2 ex: `invader deploy site`
    SITE=$2
    # IFS = delimiter, read file line by line to extract site=path
    # Accessible via ${line[0]} ${line[1]}
    while IFS="=" read -ra line; do
        if [ "$SITE" == "${line[0]}" ]
        then
            path=${line[1]}
            break
        fi
    done < $LIBFILE

    if [ -z $path ]
    then
        echo "This site does not exists in invader library.."
        exit
    fi

    echo "Are you sure you want to deploy $SITE from $path? (y/N)"
    read deploy

    if [[ -z "$deploy" || "$deploy" == "n" || "$deploy" == "N" ]]; then
        echo "Not deploying"
        exit
    fi

    if [[ "$deploy" == "yes" || "$deploy" == "y" || "$deploy" == "Y" ]]; then
        SYNCFILE=$path/.invader
    fi

    cd $path
    LOCALIGNORE="$path/.invaderignore"
    CWD=$(basename "$path")
    echo $(pwd)
fi

# If we can't find the .invader file in the current folder
if [ ! -f "$SYNCFILE" ]
then
    echo 'No configuration file found'
    exit
fi

source $SYNCFILE

# if [ -z $COMMAND ]
# then
#     TARGET=${1:-staging}
# else
#     TARGET=${3:-staging}
# fi

if [ -z $SOURCE ] 
then
    $SOURCE=$DIR
fi

if [ $"AUTO_RUN_LSCRIPTS" == "false" ]
then
    echo "Run local pre-deploy scripts? (yes/no)"
    read locscripts
else
    locscripts="yes"
fi

if [ $locscripts == "yes" ] || [ $locscripts =="y" ]
then
    IFS=,
    QUEUE=($BEFORE_LSCRIPTS)
    for key in "${!QUEUE[@]}"; 
    do
        echo "Commande a executer ${QUEUE[$key]}, voulez-vous l'executer?(y/N)"
        read runcommand

        if [[ -z "$runcommand" || "$runcommand" == "n" || "$runcommand" == "N" ]]; then
            echo "Skipping command"
        else
            eval "${QUEUE[$key]}"
        fi
    done
    printf "Fin de l'execution des commandes pré-deploiement.\n"
fi


tput setaf 4;
if [ -z $SSHCONFIG ]
then
    # echo 'without ssh config'
    rsync -anvCrti --progress --include="*/" --exclude=".invader" --exclude='.gitignore' --exclude-from='.gitignore' --exclude-from="$BASEIGNOREFILE" --exclude-from="$LOCALIGNORE" -r --log-file="$DIR/.invaderlog" -e ssh "$SOURCE" "$SSH_USER"@"$REMOTE":"$TARGET" | grep -v ".*\.git"
else
    echo $(pwd)

    # echo 'with ssh config'
    rsync -anvCrit --progress --include="*/"  --exclude=".invader" --exclude='.invaderlog' --exclude='.git/' --exclude='.gitignore' --exclude-from='.gitignore' --exclude-from="$BASEIGNOREFILE" --exclude-from="$LOCALIGNORE" -r --log-file="$DIR/.invaderlog" -e ssh "$SOURCE" "$SSHCONFIG":"$TARGET" | grep -v ".*\.git"
fi
tput setaf 9;

printf "\n"

echo "Do you agree to this deployment? (yes/no)"
read runit


if [[ -z "$runit" || $runit == "no" || $runit == "n" ]]; then
    echo "Not deploying"
    exit
fi

echo "" > ".invaderlog"
if [ -d ".git/" ]; then
    COMMIT=$(git show -s --format=%s)
else
    COMMIT=""
fi



tput setaf 2;
if [[ $runit == "yes" || $runit == "y" || $runit == "Y" ]]; then
    echo "Deploying"

    if [ -z $SSHCONFIG ]
    then
        rsync -azPvCb --progress --exclude=".invader" --exclude='.invaderlog' --exclude='.git/' --exclude='.gitignore' --exclude-from='.gitignore' --exclude-from="$BASEIGNOREFILE" --exclude-from="$LOCALIGNORE" --exclude=".git/" --log-file=".invaderlog" -e ssh "$SOURCE" "$SSH_USER"@"$REMOTE":"$TARGET"
    else
        rsync -azPvCb --progress --exclude=".invader" --exclude='.invaderlog' --exclude='.git/' --exclude='.gitignore' --exclude-from='.gitignore' --exclude-from="$BASEIGNOREFILE" --exclude-from="$LOCALIGNORE" --exclude=".git/"  --log-file=".invaderlog" -e ssh "$SOURCE" "$SSHCONFIG":"$TARGET"        
    fi


    printf "\n"
    echo "Done deploying"
    printf "\n"

    if [[ $SENDTOSLACK != "" ]]; then
        SLACKMESSAGE=$4
        if [[ -z "$SLACKMESSAGE" ]]; then
            SLACKMESSAGE="Déploiment du site $APPNAME en $ENV ( $COMMIT )"
        fi
        curl -X POST -H 'Content-type: application/json' --data "{\"text\": \"$SLACKMESSAGE\"}" $SLACK_ENDPOINT
    else
        echo "Voulez-vous envoyer un message Slack?(y/N)"
        read SENDTOSLACK

        if [[ "$SENDTOSLACK" == "y" || "$SENDTOSLACK" == "Y" ]]; then
            echo "Entrez votre message(vide pour dernier commit)"
            read SLACKMESSAGE

            if [[ -z "$SLACKMESSAGE" ]]; then
                SLACKMESSAGE="Déploiment du site $APPNAME en $ENV ( $COMMIT )"
            fi

            curl -X POST -H 'Content-type: application/json' --data "{\"text\": \"$SLACKMESSAGE\"}" $SLACK_ENDPOINT
        fi
    fi

fi
tput setaf 1;
printf "\n"

if [ "$AUTO_RUN_LSCRIPTS" == "false" ]
then
    echo "Run local post-deploy scripts? (yes/no)"
    read locscripts
else
    locscripts="yes"
fi

if [ $locscripts == "yes" ] || [ $locscripts =="y" ]
then
    IFS=,
    QUEUE=($AFTER_LSCRIPTS)
    for key in "${!QUEUE[@]}"; 
    do
        echo "Commande a executer ${QUEUE[$key]}, voulez-vous l'executer?(y/N)"
        read runcommand

        if [[ -z "$runcommand" || "$runcommand" == "n" || "$runcommand" == "N" ]]; then
            echo "Skipping command"
        else
            eval "${QUEUE[$key]}"
        fi

        printf "Fin de l'execution des commandes post-deploiement.\n"

    done
fi


if [ -n "$AFTER_RSCRIPTS" ]
then
    ssh "$SSH_USER"@"$REMOTE" "$AFTER_RSCRIPTS"
fi

echo "$SITE Invaded."

cd $DIR
exit



